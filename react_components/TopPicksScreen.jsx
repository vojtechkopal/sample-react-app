/** @jsx React.DOM */

var React = require('react'),
    TimeCounter = require('./TimeCounter.jsx'),
    Menu = require('./Menu.jsx'),
    Router = require('react-router'),
    config = require('./config/app.jsx'),
    Link = Router.Link,
    Authentication = require('./mixins/Authentication.jsx'),
    TrackList = require('./TrackList.jsx'),
    user = require('./stores/UserStore.jsx')
    ;

module.exports =  React.createClass({
    mixins: [Authentication],
    render: function () {
        return (
            <div className="top-picks-screen screen screen-wide">
                <div className="contain">
                    <div className="logo horizontal"></div>
                    <TimeCounter deadline={config.deadline} />
                    <Menu />
                    <h2><strong>TOP 10</strong> tracks as voted for so far&hellip;</h2>

                    <div className="text-center">
                        <Link to="your-vote" className="btn btn-primary">
                            <span className="fix">Vote</span>
                        </Link>
                    </div>

                    <div className="spacer"></div>

                    <TrackList />

                </div>
            </div>
        );
    }
});