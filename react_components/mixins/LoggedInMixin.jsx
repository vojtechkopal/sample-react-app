'use strict';

var userStore = require('../stores/UserStore.jsx');

var LoggedInMixin = {
    isLoggedIn: function () {
        return userStore.isLoggedIn();
    }
};

module.exports = LoggedInMixin;