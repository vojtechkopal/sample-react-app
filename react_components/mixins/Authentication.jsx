'use strict';

var userStore = require('../stores/UserStore.jsx'),
    Login = require('../LoginScreen.jsx');


var Authentication = {
    statics: {
        willTransitionTo: function (transition) {
            if (!userStore.isLoggedIn()) {
                Login.attemptedTransition = transition;
                transition.redirect('/login');
            }
        }
    }
};

module.exports = Authentication;