/** @jsx React.DOM */

var React = require('react'),
    TimeCounter = require('./TimeCounter.jsx'),
    Menu = require('./Menu.jsx'),
    Authentication = require('./mixins/Authentication.jsx'),
    config = require('./config/app.jsx'),
    user = require('./stores/UserStore.jsx')
    Router = require('react-router'),
    Link = Router.Link
    ;

module.exports =  React.createClass({
    mixins: [Authentication],
    render: function () {
        return (
            <div className="about-screen screen screen-wide">
                <div className="contain">
                    <div className="logo horizontal"></div>
                    <TimeCounter deadline={config.deadline} />
                    <Menu />
                    <h2>How to vote</h2>

                    <p className="text-center">You will see a list of 50 song titles and bands with the word 'White' included within them. Simply take a browse through the list and select your top 10 by ticking the box next to the track. You can listen to a 30 second sample of the tune by clicking on the play button.</p>

                    <p className="text-center">Once you have submitted your choices you can always log back in to change them if you get the urge!</p>

                    <div className="spacer"></div>

                    <div className="text-center">
                        <Link className="btn btn-primary" to="your-vote"><span className="fix">Vote</span></Link>
                    </div>

                    <div className="spacer"></div>

                    <div className="text-center">
                        <div className="spotify"></div>
                    </div>

                </div>
            </div>
        );
    }
});