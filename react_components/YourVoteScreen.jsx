/** @jsx React.DOM */

var React = require('react'),
    Modal = require('react-modal'),
    TimeCounter = require('./TimeCounter.jsx'),
    TrackList = require('./TrackList.jsx'),
    config = require('./config/app.jsx'),
    req = require('./utils/apiRequest.jsx'),
    Menu = require('./Menu.jsx'),
    user = require('./stores/UserStore.jsx'),
    Authentication = require('./mixins/Authentication.jsx')
    ;

module.exports =  React.createClass({
    mixins:[Authentication],
    getInitialState: function () {
        return {
            submitModalIsOpen: false,
            thankYouModalIsOpen: false,
            selectedTracks: []
        }
    },
    onSelectionChanged: function (selectedTracks, preventModal) {
        this.setState({
            selectedTracks: selectedTracks
        });

        if (!preventModal && selectedTracks.length == 10) {
            this.openSubmitModal();
        }
    },

    componentWillMount: function () {

        Modal.setAppElement(document.getElementsByTagName('body')[0]);
        Modal.injectCSS();

    },
    closeSubmitModal: function () {
        this.setState({submitModalIsOpen: false})
    },
    openSubmitModal: function () {
        this.setState({submitModalIsOpen: true})
    },
    closeThankYouModal: function () {
        this.setState({thankYouModalIsOpen: false})
    },
    openThankYouModal: function () {
        this.setState({thankYouModalIsOpen: true})
    },
    submit: function () {

        var ctx = this;
        req.post('/user/me/vote', {id: user.getId(), tracks: this.state.selectedTracks}).secure(user)
            .end(function(res){

                if (res.body!==null &&
                    typeof res.body.status !== 'undefined' &&
                    res.body.status == 0 ) {

                    ctx.refs.trackList.refreshData();
                    ctx.closeSubmitModal();
                    ctx.openThankYouModal();

                } else if (res.body!==null &&
                    typeof res.body.status !== 'undefined' &&
                    res.body.status == 1) {

                    ctx.closeSubmitModal();

                } else {

                    ctx.closeSubmitModal();

                }
            }, user);

    },
    render: function () {
        var daysLeft = Math.floor(((new Date(config.deadline)) - new Date()) / (24 * 3600 * 1000));
        var remaining = 10 - this.state.selectedTracks.length;

        var remainingDiv = remaining == 0
            ?<span>edit selection</span>
            :<span>{remaining} remaining</span>;

        return (
            <div className="your-vote-screen screen screen-wide">
                <div className="contain">
                    <div className="logo horizontal"></div>
                    <TimeCounter deadline={config.deadline} />
                    <Menu />
                    <h2>Select your<br/><strong>TOP 10</strong> tracks ({remainingDiv}):</h2>

                    <div className="spacer"></div>

                    <TrackList vote="1" ref="trackList" onSelectionChanged={this.onSelectionChanged} maxSelectedTracks="10" />
                </div>
                <Modal
                isOpen={this.state.thankYouModalIsOpen}
                onRequestClose={this.closeThankYouModal}
                className="Modal__Bootstrap modal-dialog centered"
                >
                    <div className="logo"></div>
                    <h1>Thank you for your<br/> vote</h1>
                    <p className="blue text-center">
                        Don't forget if you change your mind you have <strong>{daysLeft}</strong> days left to edit your selection.
                    </p>

                    <div className="spacer"></div>
                    <div className="spacer"></div>

                    <div className="text-center">
                        <button className="btn btn-primary" onClick={this.closeThankYouModal}>
                            <span className="fix">OK</span>
                        </button>
                    </div>

                    <div className="spacer"></div>
                    <div className="spacer"></div>
                </Modal>
                <Modal
                isOpen={this.state.submitModalIsOpen}
                onRequestClose={this.closeSubmitModal}
                className="Modal__Bootstrap modal-dialog centered"
                >
                    <div className="logo"></div>
                    <h1>Thank you</h1>
                    <h2>You have made your selection.<br/>
                    Click on submit for your votes to count.</h2>

                    <div className="spacer"></div>

                    <div className="text-center">
                        <button className="btn btn-primary" onClick={this.submit}>
                            <span className="fix">Submit</span>
                        </button>
                    </div>

                    <div className="spacer"></div>
                    <div className="spacer"></div>
                    <hr/>
                    <div className="spacer"></div>

                    <div className="text-center">
                        <button className="btn btn-success" onClick={this.closeSubmitModal}>
                            <span className="fix">Back</span>
                        </button>
                    </div>

                    <div className="spacer"></div>
                    <div className="spacer"></div>
                </Modal>
            </div>
        );
    }
});