/** @jsx React.DOM */

var React = require('react/addons'),
    req = require('./utils/apiRequest.jsx')
    ;

module.exports =  React.createClass({
    audioObject: null,
    timer: 0,
    getInitialState: function () {
        return {
            no: '00',
            track: this.props.track.track,
            artist: this.props.track.artist,
            votes: this.props.track.votes,
            spotify_uri: this.props.track.spotify_uri,
            preview_url: this.props.track.preview_url,
            picture: null,
            selected: this.props.track.voted ? this.props.track.voted : false,
            playing: false,
            loading: false,
            time: 0
        };
    },
    play: function () {
        var self = this;
        if (this.audioObject) {
            this.audioObject.play();
        } else {
            var audioObject = new Audio(this.state.preview_url);
            audioObject.play();
            audioObject.addEventListener('ended', function () {
                self.setState({
                    'playing': false
                });
            });
            audioObject.addEventListener('pause', function () {
                self.setState({
                    'playing': false
                });
            });
            this.audioObject = audioObject;
        }

        if (this.timer == 0) {
            this.timer = setInterval(function () {
                var time = self.audioObject.currentTime;
                self.setState({
                    time: time
                });
            }, 100);
        }

        if (typeof this.props.onPlay == "function") {
            this.props.onPlay(this);
        }
    },
    pause: function () {
        if (this.audioObject) {
            this.audioObject.pause();
        }

        if (this.timer) {
            clearInterval(this.timer);
            this.timer = 0;
        }
    },
    loadPreviewUri: function () {

        this.setState({
            loading: false,
            playing: true
        });
        this.play();

    },
    handleSelectClick: function () {
        var selected = this.state.selected;

        if (!this.props.disabled || this.state.selected) {
            selected = !selected;
        }

        this.setState({
            selected: selected
        });

        if (typeof this.props.onSelected == "function") {
            this.props.onSelected(this, selected);
        }
    },
    handlePlayClick: function () {
        if (this.state.playing) {
            this.setState({
                'playing': false
            });
            this.pause();
        } else {
            if (this.state.loading) {
                this.setState({
                   'loading': false
                });
            } else {
                if (this.state.preview_uri) {
                    this.setState({
                        'playing': true
                    });
                    this.play();
                } else {
                    this.setState({
                        'loading': true
                    });
                    this.loadPreviewUri();
                }
            }
        }
    },
    getTimeStr: function (time) {

        var minutes = Math.floor(time / 60);
        var seconds = Math.floor(time) - minutes*60;

        if (minutes < 10) minutes = '0'+minutes;
        if (seconds < 10) seconds = '0'+seconds;

        return minutes + ':' + seconds;
    },
    getTimePerc: function (time) {
        if (!this.audioObject) return 0;
        return 100* time / this.audioObject.duration;
    },
    render: function () {
        var cx = React.addons.classSet;
        var imageUrl = '/assets/images/tracks/'+this.state.spotify_uri+'.jpeg';

        return (
            <div className={cx({'track-item':true, 'first-ten': this.props.index <= 10, 'your-vote': true})}>
                <div className={cx({'select-box':true, 'selected': this.state.selected})} onClick={this.handleSelectClick}></div>
                <div className="center">
                    <div className="center-inner">
                        <div className="name">
                        {this.state.artist} - {this.state.track}
                        </div>
                        <div className="time">
                        {this.getTimeStr(this.state.time)}
                        </div>
                        <div className="bar">
                            <div className="inner" style={{'width': this.getTimePerc(this.state.time) + '%'}}></div>
                        </div>
                    </div>
                </div>
                <div className="right">
                    <div className="picture">
                        <img src={imageUrl} />
                    </div>
                    <div className={cx({'play-pause': true, 'paused': !this.state.playing && !this.state.loading, 'playing': this.state.playing, 'loading': this.state.loading})}
                        onClick={this.handlePlayClick}>
                        <div className="play-icon">
                            <span className="glyphicon glyphicon-play"></span>
                        </div>
                        <div className="pause-icon">
                            <span className="glyphicon glyphicon-pause"></span>
                        </div>
                        <div className="loading-icon">
                            <span className="glyphicon glyphicon-repeat"></span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});