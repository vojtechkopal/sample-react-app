/** @jsx React.DOM */

var React = require('react');

module.exports = React.createClass({
    getInitialState: function () {
        return {
            value: null
        };
    },
    onInputChange: function (e) {
        var self = this;
        this.setState({
            value: e.target.value
        }, function () {
            if (typeof self.props.onChange == "function") {
                self.props.onChange(self.state.value);
            }
        });
    },
    check: function () {
        return this.state.value !== null && this.state.value.length > 0;
    },
    getValue: function () {
        return this.state.value;
    },
    setValue: function (value) {
        this.setState({
            value: value
        });
    },
    render: function() {

        var value = this.state.value;
        var LabelEl = (this.props.label) ?
            <label for={this.props.id}>{this.props.label}</label> : null;

        return (
            <div className="form-group">
                {LabelEl}
                <input type={this.props.id} className="form-control" ref="input" id={this.props.id} value={value} onChange={this.onInputChange} placeholder={this.props.placeholder} />
            </div>
        )
    }
});
