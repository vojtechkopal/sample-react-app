/** @jsx React.DOM */

var React = require('react/addons');
var CFCheckbox = require('react-controlfacades').CFCheckbox;


module.exports = React.createClass({
    displayName: 'FormCheckbox',
    getInitialState: function () {
        return {
            value: null,
            checked: this.props.defaultChecked
        };
    },
    onInputChange: function (e) {
        var self = this;
        this.setState({
            checked: e.target.checked
        }, function () {
            if (typeof self.props.onChange == "function") {
                self.props.onChange(self.state.checked);
            }
        });
    },
    check: function () {
        if (this.props.required) {
            return this.state.checked;
        } else {
            return true;
        }
    },
    getValue: function () {
        return this.state.checked;
    },
    renderFacade: function (props, children) {

        var cx = React.addons.classSet;

        return (
            <div className={cx({'form-checkbox-inner': true, checked: props.checked})} />
        );
    },
    render: function() {

        var Input = <CFCheckbox ref="checkbox" checked={this.state.checked} id={this.props.id} onChange={this.onInputChange} className="form-checkbox" facade={ this.renderFacade } />

        var El = (this.props.label)
            ? <label for={this.props.id}>{Input} {this.props.label}</label>
            : Input;

        return (
            <div className="checkbox cf-checkbox">
                {El}
            </div>
        )
    }
});
