/** @jsx React.DOM */

var React = require('react'),
    FormInput = require('./form/FormInput.jsx'),
    FormCheckbox = require('./form/FormCheckbox.jsx'),
    user = require('./stores/UserStore.jsx'),
    LoginScreen = require('./LoginScreen.jsx'),
    Router = require('react-router'),
    _ = require('lodash'),
    req = require('./utils/apiRequest.jsx'),
    Modal = require('react-modal'),
    Navigation = require('react-router').Navigation
    ;

module.exports = React.createClass({

    mixins: [ Navigation, Router.State ],

    getInitialState: function () {
        return {
            is_loading: false,
            disabled_submit: true,
            forgottenModalIsOpen: false,
            addEmailModalIsOpen: false,
            newPasswordModalIsOpen: this.getParams().modal == 'new-password'
        };
    },

    componentDidMount: function () {

        if ( localStorage.email ) {
            this.refs.email.setValue(localStorage.email);
        }

        if ( localStorage.password ) {
            this.refs.password.setValue(localStorage.password);
        }

        this.updateSubmitButtonDisable();

    },

    handleEmailChange: function () {

        this.updateSubmitButtonDisable();

    },

    handlePasswordChange: function (v) {

        this.updateSubmitButtonDisable();

    },

    handleCheckboxChange: function (v) {

        console.log('handleCheckboxChange', v, this.refs.remember, this.refs.remember.check());
        this.updateSubmitButtonDisable();

    },

    updateSubmitButtonDisable: function () {

        this.setState({
            disabled_submit: !(
                this.refs.password.check()
                && this.refs.email.check()
                && this.refs.remember.check()
            )});

    },

    handleForgottenClick: function () {

        if (!this.refs.email.check() || !this.refs.email.getValue()) {

            this.openAddEmailModal();

        } else {
            var ctx = this;
            req.post('/reset/send-link?email=' + this.refs.email.getValue())
                .end(function () {

                    ctx.openForgottenModal();

                });
        }

    },
    handleSubmit: function () {

        var ctx = this;

        if (!this.state.is_loading) {

            this.setState({is_loading: true}, function () {

                if (ctx.state.is_loading) {
                    var email = ctx.refs.email.getValue();
                    var password = ctx.refs.password.getValue();

                    user.login(email, password, function (loggedIn) {
                        ctx.setState({is_loading: false});

                        if (loggedIn) {

                            if (ctx.refs.remember.getValue()) {
                                localStorage.email = email;
                                localStorage.password = password;
                            } else {
                                delete localStorage.email;
                                delete localStorage.password;
                            }

                            if (LoginScreen.attemptedTransition) {
                                var transition = LoginScreen.attemptedTransition;
                                LoginScreen.attemptedTransition = null;
                                transition.retry();
                            } else {
                                ctx.replaceWith('/home');
                            }

                        } else {

                            if (typeof ctx.props.onError == "function") {
                                ctx.props.onError();
                            }

                        }
                    });
                }

            });

        }

    },
    closeForgottenModal: function () {
        this.setState({forgottenModalIsOpen: false})
    },
    openForgottenModal: function () {
        this.setState({forgottenModalIsOpen: true})
    },
    closeAddEmailModal: function () {
        this.setState({addEmailModalIsOpen: false})
    },
    openAddEmailModal: function () {
        this.setState({addEmailModalIsOpen: true})
    },
    closeNewPasswordModal: function () {
        this.setState({newPasswordModalIsOpen: false})
    },
    openNewPasswordModal: function () {
        this.setState({newPasswordModalIsOpen: true})
    },
    render: function() {

        var defaultChecked = !!localStorage.email;

        return (
            <div className="login-form">
                <FormInput type="text" id="email" ref="email" label="Username" onChange={this.handleEmailChange} />
                <FormInput type="password" id="password" ref="password" label="Password" onChange={this.handlePasswordChange} />

                <FormCheckbox id="remember" ref="remember" defaultChecked={defaultChecked} label="Remember my details" onChange={this.handleCheckboxChange} />

                <div className="spacer"></div>

                <div className="text-center">
                    <button onClick={this.handleSubmit} className="btn btn-primary" disabled={this.state.disabled_submit || this.state.is_loading}>
                        <span className="fix">{this.state.is_loading ? '...' : 'Enter'}</span>
                    </button>
                </div>

                <div className="spacer"></div>

                <hr/>

                <div className="spacer"></div>

                <div className="text-center">
                    <button onClick={this.handleForgottenClick} className="btn btn-link">
                        <span className="fix">{'Forgotten Password?'}</span>
                    </button>
                </div>

                <div className="spacer"></div>

                <div className="text-center">
                    <div className="spotify"></div>
                </div>

                <div className="spacer"></div>

                <div className="text-center">
                    <Link to="privacy" className="btn btn-link grey">
                        <span className="fix">Privacy Policy</span>
                    </Link>
                </div>

                <Modal
                isOpen={this.state.forgottenModalIsOpen}
                onRequestClose={this.closeForgottenModal}
                className="Modal__Bootstrap modal-dialog centered"
                >
                    <div className="logo"></div>
                    <h1>Check your mailbox</h1>
                    <h2>You will receive an email with further instructions</h2>

                    <div className="spacer"></div>
                    <hr/>
                    <div className="spacer"></div>

                    <div className="text-center">
                        <button className="btn btn-success" onClick={this.closeForgottenModal}>
                            <span className="fix">OK</span>
                        </button>
                    </div>

                    <div className="spacer"></div>
                    <div className="spacer"></div>
                </Modal>


                <Modal
                isOpen={this.state.addEmailModalIsOpen}
                onRequestClose={this.closeAddEmailModal}
                className="Modal__Bootstrap modal-dialog centered"
                >
                    <div className="logo"></div>
                    <h1>Enter correct email</h1>
                    <h2>First enter correct email into the login form</h2>

                    <div className="spacer"></div>
                    <hr/>
                    <div className="spacer"></div>

                    <div className="text-center">
                        <button className="btn btn-success" onClick={this.closeAddEmailModal}>
                            <span className="fix">OK</span>
                        </button>
                    </div>

                    <div className="spacer"></div>
                    <div className="spacer"></div>
                </Modal>


                <Modal
                isOpen={this.state.newPasswordModalIsOpen}
                onRequestClose={this.closeNewPasswordModal}
                className="Modal__Bootstrap modal-dialog centered"
                >
                    <div className="logo"></div>
                    <h1>Check your mailbox</h1>
                    <h2>You will receive an email with your new password</h2>

                    <div className="spacer"></div>
                    <hr/>
                    <div className="spacer"></div>

                    <div className="text-center">
                        <button className="btn btn-success" onClick={this.closeNewPasswordModal}>
                            <span className="fix">OK</span>
                        </button>
                    </div>

                    <div className="spacer"></div>
                    <div className="spacer"></div>
                </Modal>
            </div>
        )
    }
});