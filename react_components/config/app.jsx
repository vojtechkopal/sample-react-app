/** @jsx React.DOM */

var externalConfiguration = configuration;

module.exports = (function () {

    var config = {
        API: externalConfiguration.API ? externalConfiguration.API : 'http://whitebook.fbapphouse.local/api',
        deadline: externalConfiguration.deadline ? externalConfiguration.deadline : '2014-12-19'
    };

    return config;

})();