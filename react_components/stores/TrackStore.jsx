'use strict';

var  TrackStore = {

    tracks: [],
    votes: [],

    getTracks: function () {

        return this.tracks;

    },

    setTracks: function (tracks) {

        this.tracks = tracks;

    },

    getVotes: function () {

        return this.votes;

    },

    setVotes: function (votes) {

        this.votes = votes;

    }

};

module.exports = TrackStore;
