'use strict';

var req = require('../utils/apiRequest.jsx');

var UserStore = (function () {
    return {

        name: null,
        email: null,
        id: null,

        getName: function () {

            return this.name;

        },

        getEmail: function () {

            return this.email;

        },

        getToken: function () {

            var token = localStorage.token;

            if (token) {
                return JSON.parse(token).token;
            }

            return null;

        },

        setToken: function (token) {

            localStorage.token = JSON.stringify(token);

        },

        getId: function () {
            return this.id;
        },

        isLoggedIn: function () {

            var token = localStorage.token;

            if (token) {
                token = JSON.parse(token);
                console.log(token);

                if (((new Date()).getTime() - token.date_created * 1000) < 1000 * 3600 * 24) {
                    return true;
                }
            }

            return false;

        },

        setProfile: function (profile) {
            if (profile.name) this.name = profile.name;
            if (profile.email) this.name = profile.email;
        },

        onChange: function (loggedIn) {



        },

        logout: function (cb) {
            delete localStorage.token;
            if (cb) cb();
            this.onChange(false);
        },

        login: function (username, password, cb) {
            if (this.isLoggedIn()) {
                if (cb) cb(true);
                this.onChange(true);
                return;
            }

            var sendData = {
                username: username,
                password: password
            };

            var self = this;

            req.post('/auth')
                .send(sendData)
                .end(function(res){

                    if (res.body!==null &&
                        typeof res.body.status !== 'undefined' &&
                        res.body.status == 0 &&
                        res.body.token &&
                        typeof res.body.token.token === 'string') {

                        self.setToken(res.body.token);
                        self.setProfile(res.body.profile);

                        if (cb) cb(true);
                        self.onChange(true);

                    } else if (res.body!==null &&
                        typeof res.body.status !== 'undefined' &&
                        res.body.status == 1) {

                        if (cb) cb(false);
                        self.onChange(false);

                    } else {

                        if (cb) cb(false);
                        self.onChange(false);

                    }
                }, self);

        }

    }
})();


module.exports = UserStore;
