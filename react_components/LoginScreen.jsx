/** @jsx React.DOM */

var React = require('react'),
    Modal = require('react-modal'),
    LoginForm = require('./LoginForm.jsx')
    ;


module.exports = React.createClass({

    statics: {
        attemptedTransition: null
    },

    getInitialState: function () {
        return {
            sending: false,
            errorModalIsOpen: false
        };
    },

    componentWillMount: function () {

        Modal.setAppElement(document.getElementsByTagName('body')[0]);
        Modal.injectCSS();

    },

    closeModal: function () {

        this.setState({
            errorModalIsOpen: false
        })

    },
    openModal: function () {

        this.setState({
            errorModalIsOpen: true
        })

    },
    render: function() {

        return (
            <div className="login-screen screen screen-tight">
                <div className="contain">
                    <div className="logo"></div>
                    <LoginForm onError={this.openModal} />
                </div>
                <Modal
                isOpen={this.state.errorModalIsOpen}
                onRequestClose={this.closeModal}
                className="Modal__Bootstrap modal-dialog"
                >
                    <div className="logo"></div>
                    <h3>The username or password<br/>
                    you have entered is incorrect.</h3>

                    <div className="spacer"></div>

                    <hr/>

                    <div className="spacer"></div>

                    <div className="text-center">
                        <button className="btn btn-success" onClick={this.closeModal}>
                            <span className="fix">OK</span>
                        </button>
                    </div>
                    <div className="spacer"></div>
                </Modal>
            </div>
        )
    }
});