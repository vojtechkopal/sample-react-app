/** @jsx React.DOM */

var React   = require('react');
var LoginScreen = require('./LoginScreen.jsx');
var HomeScreen = require('./HomeScreen.jsx');
var PrivacyScreen = require('./PrivacyScreen.jsx');
var AboutScreen = require('./AboutScreen.jsx');
var TopPicksScreen = require('./TopPicksScreen.jsx');
var YourVoteScreen = require('./YourVoteScreen.jsx');
var Router = require('react-router');
var Route = Router.Route;
var NotFoundRoute = Router.NotFoundRoute;
var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var RouteHandler = Router.RouteHandler;
var Authentication = require('./mixins/Authentication.jsx');
var auth = require('./stores/UserStore.jsx');
var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

var App = React.createClass({

    setStateOnAuth: function (loggedIn) {
        this.setState({
            loggedIn: loggedIn
        });
    },

    componentWillMount: function () {
        auth.onChange = this.setStateOnAuth;
        auth.login();
    },

    render: function () {
        return (
            <div>
                <RouteHandler/>
            </div>
        );
    }
});

var routes = (
    <Route handler={App} path="/">
        <DefaultRoute handler={HomeScreen} />
        <Route name="home" handler={HomeScreen} />
        <Route name="login" handler={LoginScreen}>
            <Route name="login-renew" path="/login/:modal" handler={LoginScreen} />
        </Route>
        <Route name="about" handler={AboutScreen} />
        <Route name="top-picks" handler={TopPicksScreen} />
        <Route name="your-vote" handler={YourVoteScreen} />
        <Route name="privacy" handler={PrivacyScreen} />
    </Route>
);

Router.run(routes, Router.HistoryLocation, function (Handler) {
    React.render(<Handler/>, document.body);
});