
'use strict';

var sa = require('superagent')
, appConfig = require('../config/app.jsx')
, Request = sa.Request
, oldEnd = Request.prototype.end
;

/**
 * JWT middleware
 */
Request.prototype.secure = function(user) {
    
    var self    = this;

    console.log(user);

    self.set('token', user.getToken());

    return self;
    
};

/**
 * Some defaults
 */
Request.prototype.end = function (fn, user) {
    
    var self = this;
    self.type('json');
    self.set('Accept', 'application/json');

    if (self.url.indexOf('//') == 0 || self.url.indexOf('://') != -1) {

    } else {
        self.set('X-Requested-With', 'XMLHttpRequest');
        self.set('Cache-Control', 'no-cache,no-store,must-revalidate,max-age=-1');
        self.url = appConfig.API + self.url;
    }
    self.timeout(15000);
    self.on('error', function (){
        console.log('Request error');
    });
    
    oldEnd.call(self, function (res) {

        console.log(res);

        var homeLoginUrl = '/login';
        if ((res.body.error && res.body.error=='401') || res.status == '401' || res.status == '403') {
            if (window.location != homeLoginUrl)
                window.location = homeLoginUrl;

            if (user) {
                user.logout();
            }

        }
        fn.call(self, res);
        
    });

};

var RequestAPI = sa;

module.exports = RequestAPI;
