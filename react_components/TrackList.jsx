/** @jsx React.DOM */

var React = require('react'),
    req = require('./utils/apiRequest.jsx'),
    user = require('./stores/UserStore.jsx')
    _ = require('lodash'),
    IScroll = require('iscroll'),
    TrackItem = require('./TrackItem.jsx'),
    TrackVoteItem = require('./TrackVoteItem.jsx'),
    trackStore = require('./stores/TrackStore.jsx')
    ;

module.exports =  React.createClass({
    iscroll: null,
    getInitialState: function () {
        return {
            tracks: trackStore.getTracks(),
            trackListHeight: 200,
            is_loading: false,
            selectedTracks: []
        };
    },
    refreshData: function (cb) {

        var ctx = this;

        if (!this.state.is_loading) {

            req.get('/track')
                .secure(user)
                .end(function (res) {

                    ctx.setState({is_loading: false});

                    if (res.body !== null &&
                        typeof res.body.status !== 'undefined' &&
                        res.body.status == 0) {

                        trackStore.setTracks(res.body.data);
                        trackStore.setVotes(res.body.votes);

                        ctx.setState({
                            tracks: trackStore.getTracks(),
                            selectedTracks: trackStore.getVotes()
                        }, function () {
                            ctx.refreshIScroll();
                            ctx.forceUpdate();

                            if (typeof ctx.props.onSelectionChanged == "function") {
                                ctx.props.onSelectionChanged(ctx.state.selectedTracks, true);
                            }

                            if (typeof cb == "function") {
                                cb(true);
                            }
                        });

                    } else if (res.body !== null &&
                        typeof res.body.status !== 'undefined' &&
                        res.body.status == 1) {

                        //modalDialogStore.open(res.body.message, 'Error');

                        if (typeof cb == "function") {
                            cb(false);
                        }

                    } else {

                        if (typeof cb == "function") {
                            cb(false);
                        }

                    }
                }, user);

        } else {

            if (typeof cb == "function") {
                cb(false);
            }

        }
    },
    componentDidMount: function () {

        window.addEventListener('resize', this.handleResize);
        this.handleResize();
        this.refreshData();

    },
    componentWillUnmount: function () {
        this.destroyIScroll();
        window.removeEventListener('resize', this.handleResize);
    },
    handleResize: function () {
        var h = window.innerHeight - this.getDOMNode().offsetTop - 40;

        if (window.innerWidth < 720) {
            h = null;
       }

        this.setState({
            trackListHeight: h
        });

        if (h == null && this.iscroll) {
            this.destroyIScroll();
        }

        if (h != null && !this.iscroll) {
            this.initIScroll();
        }
    },
    initIScroll: function () {
        if (this.iscroll) {
            this.iscroll.destroy();
            this.iscroll = null;
        }

        this.iscroll = new IScroll('.track-list', {
            mouseWheel: true,
            scrollbars: 'custom',
            interactiveScrollbars: true
        });
    },
    destroyIScroll: function () {
        if (this.iscroll) {
            this.iscroll.destroy();
            this.iscroll = null;
        }
    },
    refreshIScroll: function () {
        if (this.iscroll) {
            this.iscroll.refresh();
        }
    },
    onItemSelected: function (item, selected) {

        var selectedTracks = this.state.selectedTracks;
        var trackId = item.props.track.id;
        var index = selectedTracks.indexOf(trackId);

        if (selected) {
            if (index != -1) {

            } else {
                selectedTracks.push(trackId);
                this.setState({
                    selectedTracks: selectedTracks
                });
            }
        } else {
            if (index != -1) {
                selectedTracks.splice(index, 1);
                this.setState({
                    selectedTracks: selectedTracks
                });
            }
        }

        if (typeof this.props.onSelectionChanged == "function") {
            this.props.onSelectionChanged(this.state.selectedTracks);
        }

    },
    lastPlayedTrack: null,
    onItemPlay: function (item) {

        if (this.lastPlayedTrack != null && this.lastPlayedTrack != item) {
            this.lastPlayedTrack.pause();
        }

        this.lastPlayedTrack = item;
    },
    render: function () {

        var self = this;
        var Items = self.props.vote ? (
            <div className="contain">
            {this.state.tracks.map(function(track, i){
                return <TrackVoteItem track={track} index={i+1} onSelected={self.onItemSelected} onPlay={self.onItemPlay} disabled={self.props.maxSelectedTracks ? self.props.maxSelectedTracks <= self.state.selectedTracks.length : false} />;
            })}
            </div>
        ) : (
            <div className="contain">
            {this.state.tracks.map(function(track, i){
                return <TrackItem track={track} index={i+1} />;
            })}
            </div>
        );

        var trackListStyles = {
            height: this.state.trackListHeight ? this.state.trackListHeight + 'px' : null
        };

        var cx = React.addons.classSet;

        return (
            <div className={cx({'track-list': true, 'top-picks': !self.props.vote })} ref="trackList" style={trackListStyles}>
            {Items}
            </div>
        );
    }
});