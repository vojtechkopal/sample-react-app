/** @jsx React.DOM */

var React = require('react'),
    TimeCounter = require('./TimeCounter.jsx'),
    Menu = require('./Menu.jsx'),
    user = require('./stores/UserStore.jsx'),
    Router = require('react-router'),
    config = require('./config/app.jsx'),
    Authentication = require('./mixins/Authentication.jsx'),
    Link = Router.Link,
    Navigation = require('react-router').Navigation;
    ;

module.exports =  React.createClass({
    mixins: [ Authentication, Navigation ],

    /*statics: {
        willTransitionTo: function (transition) {
            console.log('will transition to', transition);

        }
    },*/

    render: function () {
        return (
            <div className="home-screen screen screen-wide">
                <div className="contain">
                    <div className="logo horizontal"></div>
                    <TimeCounter deadline={config.deadline} />
                    <Menu />
                    <h2 className="text-center">Welcome to The White Book Sessions</h2>

                    <p className="text-center">After a hard year of pulling around 800 pages of specifications and information together, we thought The White Book team deserved a little R&amp;R (rest and relaxation)! So, to help celebrate The White Book's completion and forthcoming print, we thought you might want to join them in choosing some music.</p>

                    <p className="text-center">If you would like to join in then please log in and vote on your choice of the top 10 'White Book' songs.</p>

                    <p className="text-center">We can all then celebrate the online launch and imminent print together, listening to your choice of music to complement our White Book launch.</p>


                    <div className="spacer"></div>

                    <div className="text-center">
                        <Link className="btn btn-primary" to="your-vote"><span className="fix">Vote</span></Link>
                    </div>

                    <div className="spacer"></div>

                    <div className="text-center">
                        <div className="spotify"></div>
                    </div>

                    </div>
            </div>
        );
    }
});