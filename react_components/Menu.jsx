/** @jsx React.DOM */

var React = require('react'),
    Router = require('react-router'),
    Link = Router.Link;
    ;

module.exports =  React.createClass({
    render: function () {
        return (
            <div className="nav menu">
                <div className="contain">
                    <ul>
                        <li className="home">
                            <Link to="home">Home</Link>
                        </li>
                        <li className="about">
                            <Link to="about">About</Link>
                        </li>
                        <li className="top-picks">
                            <Link to="top-picks">Top Picks</Link>
                        </li>
                        <li className="your-vote">
                            <Link to="your-vote">
                                Your Vote
                                <div className="icon"></div>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
});