/** @jsx React.DOM */

var React = require('react'),
    TimeCounter = require('./TimeCounter.jsx'),
    Menu = require('./Menu.jsx'),
    Authentication = require('./mixins/Authentication.jsx'),
    config = require('./config/app.jsx'),
    user = require('./stores/UserStore.jsx')
    Router = require('react-router'),
    Link = Router.Link
    ;

module.exports =  React.createClass({
    render: function () {
        return (
            <div className="about-screen screen screen-wide">
                <div className="contain">
                    <div className="logo horizontal"></div>

                    <div className="text-center">
                        <Link className="btn btn-primary" to="home">
                            <span className="fix">Back</span>
                        </Link>
                    </div>

                    <div className="spacer"></div>

                    <h2>Privacy Policy</h2>

                    <p>BPB United Kingdom Limited trading as British Gypsum ("We" or "us") is committed to protecting your privacy. We use the personal data we collect about you for the purposes described in this policy including to provide you with sales information and a more personalised visiting experience. This privacy statement does not affect your rights under Data Protection legislation.</p>
                    <p>This privacy policy tells you:</p>
                    <ul>
                        <li>What personal data of yours is collected;</li>
                        <li>With whom such data may be shared;</li>
                        <li>Our security procedures to protect against loss, misuse, or alteration of personal data under our control;</li>
                        <li>Your rights in connection with your personal data; and</li>
                        <li>How you can correct any errors in the personal data we hold on you.</li>
                    </ul>

                    <p>This policy (together with our terms of use and any other documents referred to in it) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it.</p>
                    <p>For the purpose of the Data Protection Act 1998 (the "Act"), the data controller is BPB United Kingdom Limited of Saint-Gobain House, Binley Business Park, Coventry, CV3 2TT.</p>

                    <h3>The information we collect</h3>

                    <p>The personal data we collect about you includes the following:</p>
                    <ul>
                        <li>Information that you provide by filling in forms on our site www.british-gypsum.com and/or our mobile applications (any of these and/or all will be referred to throughout this document as "our site”). This includes information provided at the time of registering to use our site, subscribing to our service, posting material or requesting further services. We may also ask you for information when you enter a competition or promotion sponsored by us, and when you report a problem with our site.</li>
                        <li>If you contact us, we may keep a record of that correspondence.</li>
                        <li>We may also ask you to complete surveys that we use for research purposes, although you do not have to respond to them.</li>
                        <li>Details of your visits to our site including, but not limited to, traffic data, location data, weblogs and other communication data, whether this is required for our own billing purposes or otherwise and the resources that you access.</li>
                    </ul>

                    <h3>How we use the information</h3>

                    <p>We use personal data held about you in the following ways:</p>
                    <ul>
                        <li>To personalise your visit to our site;</li>
                        <li>To ensure that content from our site is presented in the most effective manner for you and for your computer;</li>
                        <li>To provide you with information, products or services that you request from us or which we feel may interest you, where you have consented to be contacted for such purposes;</li>
                        <li>To carry out our obligations arising from any contracts entered into between you and us;</li>
                        <li>To allow you to participate in interactive features of our service, when you choose to do so;</li>
                        <li>To notify you about changes to our service and about important functionality changes to our site;</li>
                        <li>For the prevention and detection of fraud; and</li>
                        <li>When you enter an on-line contest or promotional feature, to administer the contest or promotion and notify winners. Each promotion will be subject to its own terms and conditions but this privacy policy will cover each promotion.</li>
                    </ul>

                    <p>We are committed to providing you with information, products and services which you require and as a valued customer we would like to keep you informed about new product launches, branch developments, new services, newsletters and bulletins and special promotions we think you will find interesting. We may also share your data with other companies in our group or third parties to provide you with information about goods and services which may be of interest to you and we or they may contact you about these by post or telephone. In the event that we wish to make any such communications electronically (e.g. via email), we may also seek your consent by asking you to tick the relevant box situated on the form on which we collect your data (e.g. the registration form or the credit account application form).</p>
                    <p>From time to time, this site may include links to other websites. We may not be operating the linked sites and we do not endorse the contents, views or products of the linked sites. We do not accept any responsibility for use of the linked sites. You are advised to use linked sites with caution and with due regard to any terms of use on those sites. If you transfer to other sites by using a link from our site to theirs we may receive aggregate or otherwise anonymous statistical information about your trip to these sites. We treat this information with the same care as we treat other information that you entrust to us but we are not responsible for the privacy policies or content of such website and we do not accept any responsibility or liability for these policies or content. Please check the privacy policy of each website you visit before you submit any personal data to these websites.</p>
                    <p>For training purposes, some telephone calls may be monitored.</p>

                    <h3>Your rights</h3>
                    <p>You may request copies of the personal data we hold about you. Any access request may be subject to a fee of £10 to meet our costs in responding to the request. Please send such requests to: British Gypsum, Head Office, East Leake, Loughborough, Leicestershire, LE12 6HX. If you have any questions, requests and/or comments about data privacy and/or this privacy policy, send an email to bgtechnical.enquiries@bpb.com.</p>
                    <p>If you believe that any personal data we hold about you is inaccurate, you should write to us at British Gypsum, Head Office, East Leake, Loughborough, Leicestershire, LE12 6HX or email us atbgtechnical.enquiries@bpb.com, telling us what you believe is wrong with your information and what you believe should be done to correct it.</p>
                    <p>You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your personal data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your personal data. You can also exercise the right at any time by contacting us at British Gypsum, Head Office, East Leake, Loughborough, Leicestershire, LE12 6HX.</p>

                    <h3>Who holds the data we collect?</h3>

                    <p>We may send your personal data and store it internationally (occasionally including countries outside the European Economic Area ("EEA")). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff may be engaged in, among other things, the fulfilment of your order, the processing of your payment details and the provision of support services. Some countries outside the EEA may not offer the same level of protection of your information. Generally, however, we will not store or send your data to anyone outside the EEA unless we have taken steps to ensure that the data will be protected in the same or equally secure way to that provided in England and Wales by the Act. By submitting your personal data, you agree to this transfer, storing or processing.</p>
                    <h3>We protect your information</h3>
                    <p>We are committed to the security of our customers' information and personal data. We take precautions to protect all of the customer personal data we have collected as you would expect of a prudent and responsible organisation.</p>
                    <p>However, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk.</p>
                    <p>Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p>
                    <p>Except in the case of fraud, death or personal injury resulting from our negligence, we do not, to the extent permitted at law, accept responsibility for or any liability resulting from any security breaches which may occur.</p>
                    <h3>We use cookies</h3>
                    <p>We use cookies on our sites. For more information about cookies and how we use them, please go to our cookie policy.</p>
                    <p>We may disclose your information to outside parties</p>
                    <p>We may disclose your personal data to any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in section 1159 of the Companies Act 2006.</p>
                    <p>At present we do not sell, trade, or rent your personal data to any party outside of our group of companies, although we may choose to do so in the future with trustworthy third parties. If you would rather we did not share your personal data please tell us not to by contacting bgtechnical.enquiries@bpb.com. If you use more than one email address to visit us, send this message from each email account you use.</p>
                    <p>Also, we may provide aggregate statistics about our customers, sales, browser type, operating system, Internet, domain, demographic and profile data, traffic patterns, and related site information to reputable third parties, but these statistics will not include personally identifying information.</p>
                    <p>As customer satisfaction is important to us, we may ask a third party research company to contact you for the sole purpose of gathering general information and specific information relating to us and our products and services.</p>
                    <p>We may also disclose your personal data to third parties where legally permitted to do so including:</p>
                    <ul>
                        <li>In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;</li>
                        <li>If we or substantially all of our assets are acquired by a third party, in which case personal data held by us about our customers will be one of the transferred assets;</li>
                        <li>For the purposes set out in clause 3; and</li>
                        <li>If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our terms of use or terms and conditions of supply and other agreements; or to protect the rights, property, or safety of us, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</li>
                    </ul>
                    <h3>Children</h3>
                    <p>We are especially interested in protecting the safety and privacy of young people using our services. We do not intend to collect information about children and recommend that children under the age of 18 seek their parents' consent before giving out personal data or using our services. Orders should not be placed through our site unless the person placing the order is over the age of 18.</p>
                    <p>We recommend that parents participate on their child's behalf in on-line browsing and purchasing experiences and supervise exploration of the Internet. Parents should be aware that our site may contain information or promote products which are not suitable for children under the age of 18.</p>
                    <h3>Your obligations</h3>
                    <p>You shall comply with the Act (including any subordinate legislation made under it and guidance issued by the Information Commissioner) and any related legislation in relation to any personal data provided to us or held by or on behalf of you as a result of or in connection with or necessary for the use of our site.</p>
                    <h3>Changes to our privacy policy</h3>
                    <p>Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail.</p>

                    <div className="spacer"></div>

                    <div className="text-center">
                        <Link className="btn btn-primary" to="home">
                            <span className="fix">Back</span>
                        </Link>
                    </div>

                    <div className="spacer"></div>

                    <div className="text-center">
                        <div className="spotify"></div>
                    </div>

                </div>
            </div>
        );
    }
});