/** @jsx React.DOM */

var React = require('react')
    ;

module.exports =  React.createClass({
    timer: 0,
    getCurrentTime: function (deadline) {
        var diff = (deadline - new Date());
        if (diff < 0) diff = 0;
        var diffDays = Math.floor(diff / (1000 * 60 * 60 * 24));
        var diffHours = Math.floor(diff / (1000 * 60 * 60)) - diffDays * 24;
        var diffMinutes = Math.floor(diff / (1000 * 60)) - diffDays * 24 * 60 - diffHours * 60;

        if (diffDays < 10) diffDays = '0'+diffDays;
        if (diffHours < 10) diffHours = '0'+diffHours;
        if (diffMinutes < 10) diffMinutes = '0'+diffMinutes;

        return {
            days: diffDays,
            hours: diffHours,
            minutes: diffMinutes
        };
    },
    getInitialState: function () {
        var deadline = new Date(this.props.deadline);
        var state = this.getCurrentTime(deadline);
        state.deadline = deadline;
        return state;
    },
    componentDidMount: function () {
        var self = this;
        this.timer = setInterval(function () {
            self.setState(self.getCurrentTime(self.state.deadline))
        }, 100);
    },
    componentWillUnmount: function () {
        clearInterval(this.timer);
    },
    render: function () {
        return (
            <div className="time-counter">
                <div className="contain">
                    <div className="row">
                        <div className="col-xs-4">
                            <div className="number" ref="days">{this.state.days}</div>
                        </div>
                        <div className="col-xs-4">
                            <div className="number" ref="hours">{this.state.hours}</div>
                        </div>
                        <div className="col-xs-4">
                            <div className="number" ref="minutes">{this.state.minutes}</div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-4">
                            <div className="sublabel">
                                Days
                            </div>
                        </div>
                        <div className="col-xs-4">
                            <div className="sublabel">
                                Hours
                            </div>
                        </div>
                        <div className="col-xs-4">
                            <div className="sublabel">
                                Mins
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="text-center time-to-vote">
                            Left to vote
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});