/** @jsx React.DOM */

var React = require('react')
    ;

module.exports =  React.createClass({
    getInitialState: function () {
        return {
            no: '00',
            track: this.props.track.track,
            artist: this.props.track.artist,
            votes: this.props.track.votes,
            picture: this.props.track.picture,
            spotify_uri: this.props.track.spotify_uri
        };
    },
    render: function () {
        var imageUrl = '/assets/images/tracks/'+this.state.spotify_uri+'.jpeg';
        var cx = React.addons.classSet;

        var votesDiv = this.state.votes == 1
            ? <div className="votes">1 vote</div>
            : <div className="votes">{this.state.votes}&nbsp;votes</div>;

        return (
            <div className={cx({'track-item':true, 'first-ten': this.props.index <= 10})}>
                <div className="no">{this.props.index}</div>
                <div className="left">
                    <div className="name">
                    {this.state.track}<br/>
                    {this.state.artist}
                    </div>
                </div>
                <div className="right">
                    <div className="picture">
                        <img src={imageUrl} />
                    </div>
                    {votesDiv}
                </div>
            </div>
        );
    }
});