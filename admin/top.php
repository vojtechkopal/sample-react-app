<?php
    require_once(dirname(__FILE__).'/header.php');
?>
<h2>TOP 10</h2>


    <hr />

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Track</th>
            <th>Artist</th>
            <th>Votes</th>
        </tr></thead>
        <tbody>
<?php $i = 0; foreach (dibi::fetchAll('SELECT t.*, COUNT(v.id) as votes FROM [track] as t LEFT JOIN [vote] as v ON (t.id = v.track_id) GROUP BY t.id ORDER BY votes DESC') as $item): ?>
            <tr class="<?php echo $i < 10 ? 'success' : '' ?>">
                <td><?php echo $item->id; ?></td>
                <td><?php echo strip_tags($item->track); ?></td>
                <td><?php echo strip_tags($item->artist); ?></td>
                <td><?php echo $item->votes; ?></td>
            </tr>
<?php $i++; endforeach; ?>
        </tbody>
    </table>

<?php
    require_once(dirname(__FILE__).'/footer.php');
?>