<?php

require_once(dirname(__FILE__).'/dibi.min.php');
require_once(dirname(__FILE__).'/config.php');
require_once(dirname(__FILE__).'/functions.php');

// connect to the DB
dibi::connect(array(
    'driver'   => 'mysql',
    'host'     => DB_HOST,
    'username' => DB_USER,
    'password' => DB_PSWD,
    'database' => DB_NAME,
    'charset'  => 'utf8',
));


$results = dibi::fetchAll('SELECT account.id, account.email, account.name, account.department, account.token_date_created, MAX(v.datetime_inserted) as voted_at FROM [account] LEFT JOIN [vote] AS v ON (v.account_id = account.id) GROUP BY account.id');

$array = array();

foreach ($results as $record) {
    $array[] = $record;
}

responseCSV($array);
