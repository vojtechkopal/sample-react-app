<?php

function smartShortUrl($url, $len = 30) {
    if (strlen($url) > $len) {
        return substr($url,0,13) . ' &hellip; ' . substr($url,strlen($url) - 13);
    } else {
        return $url;
    }
}

function responseCSV($array) {
    if (count($array) > 0) {
        $headings = array();
        foreach ($array[0] as $k => $v) {
            $headings[] = $k;
        }

        // Open the output stream
        $fh = fopen('php://output', 'w');

        // Start output buffering (to capture stream contents)
        ob_start();

        fputcsv($fh, $headings, ";");

        // Loop over the * to export
        if (! empty($array)) {
            foreach ($array as $item) {
                fputcsv($fh, array_values((array)$item), ";");
            }
        }

        // Get the contents of the output buffer
        $string = ob_get_clean();

        $filename = 'csv_' . date('Ymd') .'_' . date('His');

        // Output CSV-specific headers

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$filename.csv" );
        header("Content-Transfer-Encoding: binary");

        exit($string);

    }

    exit;
}