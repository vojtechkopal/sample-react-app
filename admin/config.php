<?php

if (file_exists(dirname(__FILE__).'/config.local.php')) {

    include_once(dirname(__FILE__) . '/config.local.php');

} else {

    define('DB_NAME', 'whitebook');
    define('DB_HOST', 'localhost');
    define('DB_PSWD', 'whitebook');
    define('DB_USER', 'whitebook');

}