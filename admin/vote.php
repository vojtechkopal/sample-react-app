<?php
    require_once(dirname(__FILE__).'/header.php');

    $user = dibi::fetch('SELECT * FROM [account] WHERE id = %i', $_GET['user']);

?>
<h2>Votes for <?php echo $user->name; ?></h2>


    <hr />
    <div>
        <a href="index.php" class="btn btn-danger">Back to Users</a>
    </div>
    <hr />

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Track</th>
            <th>Artist</th>
            <th>Voted at</th>
        </tr></thead>
        <tbody>
<?php foreach (dibi::fetchAll('SELECT t.*, v.datetime_inserted FROM [vote] as v LEFT JOIN [track] as t ON (t.id = v.track_id) WHERE v.account_id = %i ORDER BY t.id', $user->id) as $item): ?>
            <tr>
                <td><?php echo $item->id; ?></td>
                <td><?php echo strip_tags($item->track); ?></td>
                <td><?php echo strip_tags($item->artist); ?></td>
                <td><?php echo $item->datetime_inserted; ?></td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>

<?php
    require_once(dirname(__FILE__).'/footer.php');
?>