<?php

require_once(dirname(__FILE__).'/dibi.min.php');
require_once(dirname(__FILE__).'/config.php');
require_once(dirname(__FILE__).'/functions.php');

$IMAGE_DIR = dirname(__FILE__).'/../data/images';
$IMAGE_URL = WEB_URL.'/data/images';

// connect to the DB
dibi::connect(array(
    'driver'   => 'mysql',
    'host'     => DB_HOST,
    'username' => DB_USER,
    'password' => DB_PSWD,
    'database' => DB_NAME,
    'charset'  => 'utf8',
));

?><!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>CMS</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>

    <script type="text/javascript">
        tinymce.init({
            selector: ".tinymce-text",
            toolbar: "undo redo | bold italic | bullist numlist | link unlink | hr | styleselect | removeformat | code",
            menubar : false,
            plugins: ["code", "hr", "link"],
            style_formats: [
                {title: 'Header', block: 'h4'},
            ]
        });
    </script>
</head>
<body>

<div class="container">

    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Content Management System</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Users</a></li>
                    <li><a href="top.php">TOP 10</a></li>
                </ul>
            </div>
        </div>
    </nav>