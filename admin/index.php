<?php
    require_once(dirname(__FILE__).'/header.php');

?>
<h2>Users</h2>

    <hr />

    <div>
        <a href="csv.php" class="btn btn-success">Download as CSV</a>
    </div>

    <hr />

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>E-mail</th>
            <th>Name</th>
            <th>Department</th>
            <th>Last login</th>
            <th>Last voted ad</th>
            <th>&nbsp;</th>
        </tr></thead>
        <tbody>
<?php foreach (dibi::fetchAll('SELECT account.*, MAX(v.datetime_inserted) as voted_at FROM [account] LEFT JOIN [vote] AS v ON (v.account_id = account.id) GROUP BY account.id') as $item): ?>
            <tr>
                <td><?php echo $item->id; ?></td>
                <td><?php echo strip_tags($item->email); ?></td>
                <td><?php echo strip_tags($item->name); ?></td>
                <td><?php echo strip_tags($item->department); ?></td>
                <td><?php echo $item->token_date_created; ?></td>
                <td><?php echo $item->voted_at; ?></td>
                <td><a href="vote.php?user=<?php echo $item->id; ?>" class="btn-success btn btn-xs">Show votes</a></td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>

<?php
    require_once(dirname(__FILE__).'/footer.php');
?>