<?php

namespace App;

use Nette\Database\Context;

class BaseModel {

    protected $db;

    public function __construct(Context $db) {
        $this->db = $db;
    }


}