<?php

namespace App;

use dibi, DateTime, DateInterval;
use Nette\Database\Context;

class Tracks extends BaseModel {

    public function getAll() {

        return $this->db->fetchAll('SELECT t.*, COUNT(v.id) AS votes FROM track AS t LEFT JOIN vote AS v ON (v.track_id = t.id) GROUP BY t.id ORDER BY votes DESC');

    }

    public function update($track, $preview_url) {

        $this->db->query('UPDATE track SET preview_url = ? WHERE id = ?', $preview_url, $track->id);

    }

}
