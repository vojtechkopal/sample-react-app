<?php

namespace App;

use dibi, DateTime, DateInterval;
use Nette\Database\Context;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class Mails extends BaseModel {

    public function sendLink($email, $link) {

        $mail = new Message;
        $mail->setFrom('BG The White Book Sessions <mail@whitebook.fbapphouse.com>')
            ->addTo($email)
            ->setSubject('BG The White Book Sessions - Request for Password Reset')
            ->setHtmlBody(
                "<p>Hello</p><p>Someone, probably you have requested a new password for BG The White Book Sessions.</p>" .
                "<p>To reset your password please click following link or copy it to the browser:</p>" .
                "<a href=\"$link\">$link</a>"
            );

        $mailer = new SendmailMailer;
        $mailer->send($mail);

    }

    public function sendNewPassword($email, $password) {

        $mail = new Message;
        $mail->setFrom('BG The White Book Sessions <mail@whitebook.fbapphouse.com>')
            ->addTo($email)
            ->setSubject('BG The White Book Sessions - New Password')
            ->setHtmlBody(
                "<p>Hello</p><p>You have requested new password.</p>" .
                "<p>Here it is:</p>" .
                "<p><strong>$password</strong></a>"
            );

        $mailer = new SendmailMailer;
        $mailer->send($mail);

    }

}
