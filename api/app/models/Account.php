<?php

namespace App;

use dibi, DateTime, DateInterval;

class Account extends BaseModel {

    const TYPE_TRAINEE = 1;
    const TYPE_PROVIDER = 2;
    const TYPE_EMPLOYER = 3;

    protected $tokenSecret = 'helloworld';

    public function encode($object) {
        return \JWT::encode($object, $this->tokenSecret);
    }

    public function decode($jwt) {
        return \JWT::decode($jwt, $this->tokenSecret);
    }

    public function computeHashForPassword($password, $salt) {
        return crypt($password, "$2a$12$".$salt);
    }

    /**
     * @param $user
     * @param $password
     * @throws AccountException
     */
    public function setPasswordGenerator($user, $password) {

        if (!$password) {
            throw new AccountException("Missing password.");
        }

        $salt = bin2hex(openssl_random_pseudo_bytes(32));
        $hash = $this->computeHashForPassword($password, $salt);

        $user->hash = $hash;
        $user->salt = $salt;

        return $user;
    }

    /**
     * Confirm email address for given hash. Set hash to NULL afterwards.
     *
     * @param $confirm_hash
     * @throws AccountException
     */
    public function confirmEmail($confirm_hash) {

        $user = $this->db->fetch('SELECT * FROM account WHERE email_confirmation_hash = ?', $confirm_hash);
        if ($user) {
            $this->db->query('UPDATE account SET',array(
                'email_confirmation_hash' => null,
                'email_confirmed' => 1,
            ),'WHERE id = ?', $user->id);
        } else {
            throw new AccountException("This confirmation code does not work");
        }

        return $user;

    }

    /**
     *
     *
     *
     * @param $user
     * @param $password
     */
    public function registerGenerator($user, $password) {

        if (!$user->email) throw new AccountException("Missing username");

        $existingUser = $this->db->fetch('SELECT * FROM account WHERE email = LOWER(?)', strtolower($user->email));
        if ($existingUser) {
            throw new AccountException("User already exists with email ".$user->email);
        }

        $user = $this->setPasswordGenerator($user, $password);
        $user = $this->generateEmailConfirmationHash($user);

        try {

            $user->email = strtolower($user->email);
            $this->db->query('INSERT INTO account', (array)$user);
            $user->id = $this->db->getInsertId();

        } catch (\Exception $e) {

            throw new AccountException("Cannot save account: ".$e->getMessage()." ".$e->getTraceAsString());

        }

        return $user;

    }

    public function bindParams($user, $data) {

        if (is_null($user)) {
            throw new AccountException("User is empty");
        }
        if (is_null($data) || count($data) == 0) {
            throw new AccountException("Data is empty");
        }

        $allow = array('tutor',
            'employer', 'provider', 'name', 'full_name',
            'addressLineOne', 'addressLineTwo', 'city', 'postCode', 'phone',  'company', 'position', 'approved');

        foreach ($data as $k => $v) {
            if (!in_array($k, $allow)) {
                throw new AccountException("Cannot bind param ".$k);
            }
            $user[$k] = $v;
        }

        try {
            $this->db->query('UPDATE account SET', (array)$data, 'WHERE id = ?', $user->id);
        } catch (\Exception $e) {
            throw new AccountException("Couldn't update user data: ".$e->getMessage()." ".$e->getTraceAsString());
        }

        return $user;

    }

    public function bindTutor($user, $selectedTutor) {

        $this->db->query('UPDATE account SET',array(
            'tutor_id'=>$selectedTutor,
            'approved'=>0
        ),'WHERE id = ?', $user->id);
        $user->tutor_id = $selectedTutor;
        $user->approved = 0;
        return $user;

    }

    public function bindProvider($user, $selectedProvider) {

        $this->db->query('UPDATE account SET', array(
            'provider_id' => $selectedProvider,
            'tutor_id' => null,
            'approved' => 0
        ), 'WHERE id = ?', $user->id);
        $user->provider_id = $selectedProvider;
        $user->tutor_id = null;
        $user->approved = 0;
        return $user;

    }

    /**
     * @param $email
     * @throws AccountException
     */
    public function createUserToken($email) {


        $user = $this->db->fetch('SELECT * FROM account WHERE LOWER(email) = ?', strtolower($email));

        if (!$user) {
            throw new AuthException("User does not exist");
        }

        if (!$user->email_confirmed) {
            throw new AuthException("You need to verify your email first");
        }

        $now = new DateTime();
        $token = (object)array(
            'token'=>$this->encode((object)array('email'=>$email, 'iat'=>$now->format('U'), 'ttl'=>3600*1000*24)),
            'date_created'=> $now->format('U')
        );

        $this->db->query('UPDATE account SET', array(
            'token' => $token->token,
            'token_date_created' => $now
        ), 'WHERE id = ?', $user->id);

        $tokenRes = new \stdClass();
        $tokenRes->user = $user;
        $tokenRes->token = $token;
        $tokenRes->type = $user->type;

        return $tokenRes;

    }

    public function invalidateUserToken($email) {
        throw new AccountException("Not implemented.");
    }

    public function generateResetToken($email) {

        $user = $this->findUserByEmailOnly($email);
        $reset_token = bin2hex(openssl_random_pseudo_bytes(4));
        $reset_token_expiration = date('U') + 2*60*60;

        $reset_hash = $this->computeHashForPassword($reset_token, $user->salt);

        $this->db->query('UPDATE account SET', array(
            'reset_token' => $reset_hash,
            'reset_token_expires' => $reset_token_expiration
        ),'WHERE id = ?', $user->id);

        return $reset_token;

    }

    public function findUserForResetToken($hash) {

        $user = $this->db->fetch('SELECT * FROM account WHERE reset_token = ? AND reset_token_expires < NOW()', $hash);

        if (!$user) {
            throw new AccountException("User does not exists");
        }

        return $user;

    }

    public function generateNewPasswordFor($user) {

        $password = bin2hex(openssl_random_pseudo_bytes(4));
        $user = $this->setPasswordGenerator($user, $password);

        $this->db->query('UPDATE account SET salt = ?, hash = ? WHERE id = ?', $user->salt, $user->hash, $user->id);
        unset($user->salt);
        unset($user->hash);

        return $password;

    }

    public function generateEmailConfirmationHash($user) {
        $user->email_confirmation_hash = bin2hex(openssl_random_pseudo_bytes(32));
        return $user;
    }

    public function get($id) {
        $user = $this->db->fetch('SELECT * FROM account WHERE id = ?', $id);
        if (!$user) {
            throw new AccountException("User does not exists");
        }
        return $user;
    }

    public function findUser($email, $token) {

        $user = $this->db->fetch('SELECT * FROM account WHERE LOWER(email) = ? AND token = ?', strtolower($email), $token);

        if (!$user) {
            throw new AccountException("User does not exists");
        }

        return $user;

    }

    public function findUserByEmailOnly($email) {

        $user = $this->db->fetch('SELECT * FROM account WHERE LOWER(email) = ?', strtolower($email));

        if (!$user) {
            throw new AccountException("User does not exists");
        }

        return $user;

    }

    public function getProfile($_user) {
        if (!$_user) {
            return null;
        }

        $user = clone $_user;

        $allow = array('email', 'tutor',
            'employer', 'provider', 'name', 'type', 'filled',
            'addressLineOne', 'addressLineTwo', 'city', 'postCode', 'phone',  'company', 'position', 'approved',
            'notificationLength', 'initialAssessmentCompleted', 'tutor', 'provider');

        $arr = (array)$user;
        $keys = array_keys($arr);
        $removeKeys = array_diff($keys, $allow);

        foreach ($removeKeys as $k) {
            unset($arr[$k]);
        }

        return (object)$arr;
    }

    /**
     * @param $token
     * @throws AccountException
     * @throws AuthException
     * @returns user
     */
    public function authToken($token) {

        if (!$token) {
            throw new AuthException("Missing token in header");
        }

        try {
            $decoded = $this->decode($token);
        } catch (\Exception $e) {
            throw new AuthException("Cannot decode the token: ".$e->getMessage());
        }

        if ($decoded && $decoded->email) {

            $user = $this->findUser($decoded->email, $token);

            if (is_null($user->token)) {
                throw new AuthException("Unknown token.");
            } else {

                $now = new DateTime();
                $date_created = new DateTime($user->token_date_created);
                $hasExpired = ((int)$now->format('U') - (int)$date_created->format('U'))  > 60*60*24;

                if ($hasExpired) {
                    throw new AuthException("Expired token");
                } else {
                    return $user;
                }

            }

        } else {

            throw new AuthException("Invalid token");

        }

    }

    public function changePassword($user, $password) {

        $user = $this->setPasswordGenerator($user, $password);
        $this->db->query('UPDATE account SET', array(
            'hash' => $user->hash,
            'salt' => $user->salt
        ),' WHERE id = ?', $user->id);

    }

}

class AccountException extends \Exception {

}

class AuthException extends \Exception {

}