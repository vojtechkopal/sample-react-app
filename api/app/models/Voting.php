<?php

namespace App;

use dibi, DateTime, DateInterval;
use Nette\Database\Context;

class Voting extends BaseModel {

    public function voteFor($user, $track_ids)
    {

        if (!$user || !isset($user->id)) {
            throw new \Exception("Unknown user");
        }

        $this->db->table('vote')->where('account_id = ?', $user->id)->delete();

        foreach ($track_ids as $id) {
            $this->db->table('vote')->insert(array(
                'account_id' => $user->id,
                'track_id' => $id
            ));
        }

    }

    public function getMyVotes($user) {

        if (!$user || !isset($user->id)) {
            throw new \Exception("Unknown user");
        }

        $votes = $this->db->table('vote')->where('account_id = ?', $user->id)->fetchAll();

        $ids = array();

        foreach ($votes as $vote) {
            $ids[] = $vote->track_id;
        }

        return $ids;

    }

}
