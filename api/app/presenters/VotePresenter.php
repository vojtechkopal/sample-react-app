<?php

class VotePresenter extends BasePresenter
{

    public function actionCreate() {

        if (!$this->authedUser) {

            $this->responseNotAuthorized();

        }

        $data = $this->getPost();
        $voting = new \App\Voting($this->db);
        $voting->voteFor($this->authedUser, $data->tracks);

        $this->responseJson(array(
            'status' => 0
        ));

    }

    public function actionRead()
    {
        $this->responseJson(array(
            'status' => 0,
            'params' => $this->getParameters()
        ));
    }

}