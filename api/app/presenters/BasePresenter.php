<?php

use Nette\Application\UI\Presenter;

if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
        $headers = '';
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

class BasePresenter extends Presenter
{
    protected $db;
    protected $authedUser;

    public function __construct(Nette\Database\Context $database) {

        $this->db = $database;

    }

    public function startup() {

        parent::startup();

        $token = null;
        $headers = getallheaders();
        if(isset($headers['token'])) {
            $token = $headers['token'];
        }

        if ($token) {
            // try auth user
            $account = new \App\Account($this->db);

            try {
                $user = $account->authToken($token);
                if ($user) {
                    $this->authedUser = $user;
                }

            } catch (\AuthException $e) {
                $this->responseNotAuthorized();
            } catch (\Exception $e) {
                $this->responseNotAuthorized("Error when authorizing user. " . $e->getMessage());
            }
        }

    }

    public function getPost() {

        $params = (object)$this->getParameters();
        $postData = json_decode($params->data);

        return $postData;

    }

    public function actionOptions() {

        $this->responseJson(array(
            'status' => 0,
            'params' => $this->getParameters()
        ));

    }

    public function responseJson($object, $code = 200) {
        $header = 'HTTP/1.1 ';
        switch ($code) {
            case 200: $header .= '200 OK'; break;
            case 400: $header .= '400 Bad Request'; break;
            case 404: $header .= '404 Not Found'; break;
            case 401: $header .= '401 Unauthorized'; break;
            case 402: $header .= '402 Payment Required'; break;
            default: $header .= $code;
        }
        header($header, true, $code);
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        header('Access-Control-Allow-Headers: X-Requested-With, X-Prototype-Version, Content-Disposition, Cache-Control, Content-Type, token');
        header("Content-Type: application/json");

        if (is_null($object)) $object = new \stdClass();
        echo json_encode($object);;
        exit;
    }

    public function responseNotAuthorized($message = "Not authorized") {

        $response = new \stdClass();
        $response->status = 1;
        $response->message = $message;
        $this->responseJson($response, 401);

    }

    public function responseError($message, $error = null) {

        $response = new \stdClass();
        $response->status = 1;

        if ($error) {
            $response->error = $error;
        }

        $response->message = $message;
        $this->responseJson($response);

    }

}