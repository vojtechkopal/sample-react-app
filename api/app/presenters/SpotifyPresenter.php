<?php

class SpotifyPresenter extends BasePresenter
{

    public function actionRead()
    {
        $params = (object)$this->getParameters();

        try {
            if (isset($params->id)) {

                if ($params->id == 'update') {

                    $trackService = new \App\Tracks($this->db);
                    $tracks = $trackService->getAll();

                    $updated = 0;
                    foreach ($tracks as $track) {
                        $uri = $track->spotify_uri;
                        $url = 'https://api.spotify.com/v1/tracks/' . $uri;
                        $data = file_get_contents($url);
                        $data = json_decode($data);

                        $trackService->update($track, $data->preview_url);
                        $updated++;
                    }

                    $this->responseJson((object)array('updated'=>$updated));

                } else {
                    $url = 'https://api.spotify.com/v1/tracks/' . $params->id;
                    $data = file_get_contents($url);

                    $data = json_decode($data);

                    $this->responseJson($data);
                }

            }
        } catch (\Exception $e) {

            $this->responseError($e->getMessage());

        }
    }

}