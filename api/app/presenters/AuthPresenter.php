<?php

use \App\Account;

class AuthPresenter extends BasePresenter
{

    public function actionCreate()
    {

        $postData = $this->getPost();
        $newHash = null;

        try {
            $account = new Account($this->db);

            if (isset($postData->token)) {

                $token = $postData->token->token;
                $account = new Account($this->db);

                try {
                    $user = $account->authToken($token);
                    if ($user) {
                        $this->authedUser = $user;
                    }

                    $response = new stdClass();
                    $response->status = 0;
                    $response->profile = $account->getProfile($user);
                    $response->token = $postData->token;
                    $this->responseJson($response);

                } catch (\AuthException $e) {
                    $this->responseNotAuthorized();
                } catch (\Exception $e) {
                    $response = new stdClass();
                    $response->status = 1;
                    $response->message = "Error when authorizing user.";
                    $this->responseJson($response);
                }

            } else if (isset($postData->hash)) {
                $confirmedUser = $account->confirmEmail($postData->hash);
                $tokenRes = $account->createUserToken($confirmedUser->email);
                $user = $tokenRes->user;
                $newHash = $confirmedUser->hash;
            } else if (isset($postData->username)) {
                $tokenRes = $account->createUserToken($postData->username);
                $user = $tokenRes->user;
                $newHash = $account->computeHashForPassword($postData->password, $user->salt);
            } else {
                throw new \AuthException("Cannot log in");
            }

            if ($newHash != $user->hash && ($newHash != $user->reset_token || $user->reset_token_expires < date("U"))) {
                $response = new stdClass();
                $response->status = 1;
                $response->newHash = $newHash;
                $response->user = $user;
                $response->message = "Unknown password or username.";
                $this->responseJson($response);
            }

            $response = new stdClass();
            $response->status = 0;
            $response->profile = $account->getProfile($tokenRes->user);
            $response->token = $tokenRes->token;

            $this->responseJson($response);

        } catch (\AuthException $e) {

            $response = new stdClass();
            $response->status = 1;
            $response->error = 3;
            $response->message = "Cannot log in: ".$e->getMessage();
            $this->responseJson($response);

        } catch (\Exception $e) {

            $response = new stdClass();
            $response->status = 1;
            $response->message = "Cannot log in: ".$e->getMessage();
            $this->responseJson($response);
        }
    }

}