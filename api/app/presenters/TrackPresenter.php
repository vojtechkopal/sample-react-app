<?php

class TrackPresenter extends BasePresenter
{

    public function actionRead()
    {
        $params = (object)$this->getParameters();

        if (isset($params->id)) {

            $this->responseJson(array(
                'status' => 0
            ));

        } else {

            $tracksService = new \App\Tracks($this->db);
            $tracks = $tracksService->getAll();
            $votes = array();

            if ($this->authedUser) {
                $votingService = new \App\Voting($this->db);
                $votes = $votingService->getMyVotes($this->authedUser);

                foreach ($tracks as &$track) {
                    if (in_array($track->id, $votes)) {
                        $track->voted = true;
                    }
                }
            }

            $this->responseJson(array(
                'status' => 0,
                'data' => $tracks,
                'votes' => $votes
            ));

        }
    }

}