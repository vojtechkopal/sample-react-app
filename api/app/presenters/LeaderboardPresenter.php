<?php

class LeaderboardPresenter extends BasePresenter
{

    public function actionRead()
    {
        $this->responseJson(array(
            'status' => 0,
            'params' => $this->getParameters()
        ));
    }

}