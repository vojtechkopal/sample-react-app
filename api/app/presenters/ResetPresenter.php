<?php

class ResetPresenter extends BasePresenter
{
    public function actionCreate()
    {
        $params = (object)$this->getParameters();
        $baseUrl = $this->context->parameters['app']['url'];

        if (isset($params->id)) {
            if ($params->id == 'send-link') {

                try {
                    $accountService = new \App\Account($this->db);

                    $user = $accountService->findUserByEmailOnly($_GET['email']);
                    $hash = $accountService->generateResetToken($user->email);


                    // send reset link
                    $link = $baseUrl . '/api/reset/send-password?hash=' . $hash . '&email=' . $user->email;

                    $mailService = new \App\Mails($this->db);
                    $mailService->sendLink($user->email, $link);

                    $this->responseJson(array('link' => $link));

                } catch (\Exception $e) {
                    $this->responseError("Couldn't process your request.", $e->getMessage());
                }
            }
        }
    }

    public function actionRead()
    {

        $params = (object)$this->getParameters();
        $baseUrl = $this->context->parameters['app']['url'];

        if (isset($params->id)) {
            if ($params->id == 'send-password') {

                $accountService = new \App\Account($this->db);

                $user = $accountService->findUserByEmailOnly($_GET['email']);
                $computed_hash = $accountService->computeHashForPassword($_GET['hash'], $user->salt);

                if ($computed_hash == $user->reset_token && $user->reset_token_expires > date("U")) {

                    // send new password
                    $password = $accountService->generateNewPasswordFor($user);

                    $mailService = new \App\Mails($this->db);
                    $mailService->sendNewPassword($user->email, $password);

                    header('Location: '.$baseUrl.'/login/new-password');
                    exit;

                } else {

                    $this->responseNotAuthorized();

                }

            }
        }

    }

}