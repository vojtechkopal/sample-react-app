<?php

require_once(__DIR__ . '/vendor/autoload.php');

use AdamStipak\RestRoute;

// Configure application
$configurator = new Nette\Configurator;

// Enable Nette Debugger for error visualisation & logging
$configurator->enableDebugger(__DIR__ . '/data/log');

// Create Dependency Injection container
$configurator->setTempDirectory(__DIR__ . '/data/temp');
$configurator->addConfig(__DIR__ . '/config.neon');

if (file_exists(__DIR__ . '/config.local.neon')) {
    $configurator->addConfig(__DIR__ . '/config.local.neon');
}

// Enable RobotLoader - this will load all classes automatically
$configurator->createRobotLoader()
    ->addDirectory(__DIR__ . '/libs')
    ->addDirectory(__DIR__ . '/app')
    ->register();

$container = $configurator->createContainer();

$router = $container->getService('router');
$router[] = new RestRoute();

// Run the application!
$container->getService('application')->run();
