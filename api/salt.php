<form action="" method="POST">
    <label for="password">Password:</label><input type="password" name="password"/>
    <label for="passwordAgain">Verify password:</label><input type="password" name="passwordAgain"/>
    <input type="submit" value="Generate" />
</form><br><br><?php

if (isset($_POST['password'])) {
    if (strlen($_POST['password']) > 0 && $_POST['password'] == $_POST['passwordAgain']) {
        $blowfish_salt = bin2hex(openssl_random_pseudo_bytes(22));

        echo $blowfish_salt;
        echo "<br/>";
        echo crypt($_POST['password'], "$2a$12$".$blowfish_salt);
    } else {
        if (strlen($_POST['password']) == 0) {
            echo 'Password cannot be empty.';
        } else {
            echo 'Passwords do not match.';
        }
    }
}