<?php

$arr = [
    "4Mu6ZKw3A4J1z7f1D9838q",
    "0O6VeBiFCwHrpVURJSsGsU",
    "7hnW9af7GuGH5lyUUTa8UH",
    "6RJyjSI6K6JnQaJp3NUnc3",
    "7mMaJKkvMKUB4KPtWjMQ8D",
    "1z1ePyeyeG9Bs6GbTolA07",
    "3m6KkYKdnbffMpGd9Pm9FP",
    "5X76oXHcR5uCXali0gOyX5",
    "77odIo0iBNrSO9MbMaR0XP",
    "0brrVj6VnvrG70SjRTnEJA",
    "7Jo2QpFmpm6nT5HYvSn3yE",
    "2id8E4WvczfKHB4LHI7Np3",
    "2buxHlSPzrrbMqutvf2oXW",
    "0bvpSvOYt4KTfZIZJmyFOs",
    "2xmuiupNHYH8u7fBwZqYet",
    "7EsjkelQuoUlJXEw7SeVV4",
    "2aafjfykzVovKLNbwRxjnz",
    "145MHyc6dO2iQHFGlPzUbm",
    "4kbz7rHVbyjKasuuqelccQ",
    "1RJxjF2aOMi1JLJ9K2PUJj",
    "7xo1Kn00qY08vwX8FMnb62",
    "4TVOhq8xeEuNmV3VzuWR4k",
    "0wY8tHgipYpFCQfhdCUuCP",
    "5ZK7JYtiFIDx4LqxwuPuhu",
    "2bb9n7VBCUmFJ8CdhykIwh",
    "3QrhkUeBVwbNy16Tlnb97Z",
    "7g1p3Yq7ypZfhWsbqC5mgl",
    "0xM88xobymkMgg46MStfnV",
    "51tuwnX8OPvQCXoBnjMkfF",
    "6TWbjjl3gafia6e7EiVvkj",
    "1ySdjlU5Vc24w81xam2MHR",
    "5Lf74o9BFFGIZDWNUtAIap",
    "2QMyoEEjsMwRejiTzVLlrZ",
    "0tJUTYJFeEPCzYUUk7JJld",
    "3q76upyNC5OXGuCdI4yqjQ",
    "3sPgYsbRwiSMCBXcry8NWm",
    "0WWz2AaqxLoO0fa9ou6Fqc",
    "3QvKie6yU8embOjRqM0fDH",
    "4pMhwgF4a3R61AjEc9AbEh",
    "0KcZApSbxEdUBZzyqKBUtW",
    "3uEgHZJzBqK4Co4gGUhgd7",
    "57epoi5Tab9SB8L5bdZBRF",
    "4LKiFDUTwsqX8K3Up8I8K0",
    "15HadeuRG9THazDGzLH8ZU",
    "5XtAwyLsH2ARH2ztJLawG7",
    "3CLlffDgi9y70uC3JRQVZx",
    "7zIBSt9PdDRWWJQnEHtY4K",
    "2FxnvkyxWLFXFANYWzcQhQ",
    "70bxulvjnkuu1NBeJtUK7m",
    "5u7JGFtQNT0Zj7TOykBS7s"
];

foreach ($arr as $e) {

    $f = file_get_contents('https://api.spotify.com/v1/tracks/'.$e);
    $t = json_decode($f);

    $images = $t->album->images;
    $url = $images[count($images) - 1]->url;
    echo $url . "\n";

    $im = file_get_contents($url);
    file_put_contents(__DIR__.'/assets/images/tracks/'.$e.'.jpeg', $im);

}