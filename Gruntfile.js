module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            react: {
                files: 'react_components/*.jsx',
                tasks: ['browserify']
            }
        },

        include_bootstrap: {
            development: {
                options: {
                    sourceMap: true,
                    dumpLineNumbers: 'comments',
                    relativeUrls: true
                },
                files: {
                    'assets/styles/styles.css': 'less/manifest.less',
                },
            },
            production: {
                options: {
                    cleancss: true,
                    compress: true,
                    relativeUrls: true
                },
                files: {
                    'assets/styles/styles.css': 'less/manifest.less',
                },
            }
        },

        browserify: {
            options: {
                transform: [ require('grunt-react').browserify ]
            },
            client: {
                src: ['react_components/**/*.jsx'],
                dest: 'assets/scripts/main.js',
                options: {
                    external: ['configuration']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-include-bootstrap');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'include_bootstrap:development',
        'browserify'
    ]);
};