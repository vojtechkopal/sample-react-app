<?php

// OR if (preg_match('/\.(?:png|jpg|jpeg|gif)$/',$_SERVER["REQUEST_URI"]))
if (file_exists(__DIR__ . '/' . $_SERVER['REQUEST_URI'])) {
   return false; // serve the requested resource as-is.
} else {
   include_once(__DIR__.'/index.php');
}